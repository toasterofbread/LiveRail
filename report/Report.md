This markdown file was generated automatically from [report/Report.docx](/report/Report.docx), so it may contain formatting errors.

Web demo available at: https://toasterofbread.github.io/LiveRail/

<br>

##### A-Level Computer Science Project

# LiveRail

[Problem Identification](#problem-identification)

[Suitability to a computational solution](#suitability-to-a-computational-solution)

[Existing Solutions](#existing-solutions)

[Essential Features](#essential-features)

[System Requirements](#system-requirements)

[Success Criteria](#success-criteria)

[Breaking down the problem](#breaking-down-the-problem)

[Usability Features](#usability-features)

[Interface design](#interface-design)

[Variables and data structures](#variables-and-data-structures)

[Program structure](#program-structure)

[Algorithms](#algorithms)

[Identifying test data](#identifying-test-data)

[Project scope](#project-scope)

[Parsing and displaying railway lines data](#parsing-and-displaying-railway-lines-data)

[Parsing and displaying train timetable data](#parsing-and-displaying-train-timetable-data)

[Calculating on-map train positions](#calculating-on-map-train-positions)

[Displaying trains using calculated positions](#displaying-trains-using-calculated-positions)

[Testing to inform evaluation](#testing-to-inform-evaluation)

[Stakeholder testing](#stakeholder-testing)

[Success of the solution](#success-of-the-solution)

[Usability of the product](#usability-of-the-product)

[Future maintenance and development](#future-maintenance-and-development)

[References](#references)

# Analysis

# Problem Identification

Watching trains go by is an activity that I and many other railfans
enjoy. While going to a new place can be part of the fun, it would be
convenient to be able to do so from the comfort of home. With a software
simulation of a railway, users could take a look at a railway or
specific station whenever and wherever they want, even for a far-away
location. This would be useful for looking up station or timetable
information in a fun and interactive interface, or for just enjoying the
view.

There are software applications for simulating railways, many allowing
users to control the trains or even construct new lines. The problem is
that no application that I've found is able to scratch the itch of just
enjoying the trains move on their own, without the user having to worry
about managing anything. By using freely available real-world
timetables, a program could simulate the position and movement of trains
in real time within a 3D model of the environment, allowing users to
observe a simulated world that's as close as possible to the real thing.
The simulation would appeal to many different people by allowing them to
adjust the position of the camera, as well as the simulation's speed and
time.

Stakeholders

The primary demographic for this application is railfans: people who are
interested in railways or simply enjoy viewing or taking a ride on
trains. Many railfans would already be familiar with the computer
hardware and software needed to run this simulation, because they would
use them for looking up train information and times with other programs.
Some would use this application as an alternative or replacement for
those programs, while others might enjoy using it to discover new and
interesting locations, or use it as a relaxing background for their
computer screen while they are doing something else.

The secondary demographic is people who use trains regularly, such as
for commuting to school or work, and happen to frequent a location that
can be viewed in the program. These people would use the program for
things like checking when the next train is scheduled for, but in a more
interesting interface than applications they might already be using for
this purpose while still offering similar functionality.

# Suitability to a computational solution

A computer simulation of a railway has several advantages over the real
thing. Firstly, a computer program can also be accessed at any time from
any place. This is similar in some ways to viewing a railway using a
live camera feed, but camera feeds often have limited availability and
are almost completely static. With a simulation, users can also view the
environment from any perspective and change the time to their liking.
Accessibility is particularly important if the location a user wants to
view is a long distance away or they want to view many separate
locations, as this could be very expensive and time-consuming if done in
the real world.

In addition, while the locations available in the simulation will be
limited, any number can be added if desired and at a lower cost than the
first location thanks to reused code and assets. Implementing locations
and the simulation itself will require the use of several computational
methods in particular:

### Problem recognition and decomposition

The main problem behind creating a simulation like this is correctly
using the timetable data available to infer train positions, and then
applying that information to the objects within the simulation in a way
that looks realistic. This is important because people looking for a way
to get information require accurate and relevant to their potential
journey, and those wanting an enjoyable solution may greatly value a
simulation that feels close to the real thing.

In order to realistically move a train based on the timetable, the
program will have to:

1.  Determine the train's target speed based on its travel time and the
    maximum speed of the line.

2.  Calculate how far along the track the train should be at the given
    simulation time based on its departure time, target speed, and
    acceleration.

3.  Set the position and rotation of each carriage based on the
    calculated position and the configuration of the track.

Abstraction

While accuracy is a key aim, the simulation will use a lot of
abstraction. Most notably, it won't simulate anything other than the
scheduled trains themselves. Passengers, signals, out-of-service trains,
freight trains, and anything else in the environment will be static or
absent entirely. These things are ignored for the sake of the simplicity
of the project and product, as they are not essential parts of the
solution. In addition, the simulation will use real-world timetable data
but won't necessarily reflect reality if a delay or rescheduling occurs.

Heuristics

To avoid the issues that could come with using live train position data
such as availability, in terms of both location and uptime, the
simulation will infer the positions of trains based on timetable data
and the current time. This is a trade-off sacrificing some accuracy for
a much more practical solution to the problem.

### Visualisation and data mining

Not only will the inferred train positions be displayed as 3D models
within the environment, but the timetable data itself will also be
displayed directly to the user when they click on trains or stations.
Displaying the entire timetable for that object would be hard to read,
so the program will display only a few upcoming times based on the
simulation's current time.

# Existing Solutions

![](images/image1.png){width="6.40625in"
height="3.5520833333333335in"}

"**metrogram**" is a simple top-down 2D simulation of the Tokyo Metro
that runs in a web browser. Like my planned solution, it simulates train
positions based on real timetable data. metrogram's simulation runs at a
static, faster than real-time speed and doesn't allow the camera to be
moved manually, instead periodically moving between perspectives.
Railway lines in metrogram are shown as grey lines, stations as dots,
and trains as moving coloured dots, all over a light grey background.
The UI is very clean and unobtrusive, displaying the current simulated
time and the name of the focused station (in both Japanese and English)
using a thin font. Each of these features strongly contribute to a very
consistent overall look and feel.

The core concept of metrogram is very similar to my own and, while the
final solutions are very different, the design principles applied are
also close to what I would like to use. metrogram's UI is very minimal,
meaning it can be checked for information if users want to do so but
doesn't draw the user's attention away from the simulation itself, which
is important for the simulation to be enjoyed on its own as something to
look at.

![](images/image2.png){width="3.1770833333333335in"
height="3.25in"}![](images/image3.png){width="2.9421303587051617in"
height="1.6549475065616799in"}![](images/image4.png){width="2.9247681539807524in"
height="1.6451826334208224in"}

"**OpenBVE**" is a 3D train simulator which has no built-in content,
instead allowing users to create their own scenarios (including routes,
trains, and sounds) and share them with other users. While OpenBVE,
unlike my solution, simulates the driving experience rather than having
the trains move on their own, it does seem to have a commitment to
immersion and realism. This can be seen in UI, which uses several
diegetic elements within the driver's cab such as watches, speed gauges,
and brake pressure gauges. For information that isn't displayed
dietetically, usually because having the physical indicator be visible
would make the view outside too small, OpenBVE uses small, transparent,
and straight-to-the-point indicators at the edges of the screen.

The use of diegetic UI elements is something that I would like to
include in my solution. I could simply display them in the on-screen UI,
but this might harm the minimal and unobtrusive style. Instead, I will
include these elements in appropriate submenus such as showing the time
as a railway-style watch within the menu for displaying train or station
timetable data.

OpenBVE includes several camera positioning options that can be switched
between at any time. The default option, inside the driver's cab, allows
a limited amount of camera rotation (probably due to the interior
visuals being incomplete). The other main option available places the
camera immediately outside the train's chassis, slightly above and to
the side, and allowing the user to look around freely. However, neither
of these options seemed to allow the camera to be moved horizontally or
vertically, which is a limitation I would like to avoid in my own
solution by allowing users to freely move and rotate the camera at any
time.

![](images/image5.png){width="5.016202974628172in"
height="2.8216152668416448in"}

![](images/image6.png){width="5.028934820647419in"
height="2.82877624671916in"}

"**電車でGO! ポケット大阪環状線編**" ("**Densha de GO! Pocket Osaka Loop
Line Edition**", referred to here as "**DDG**") is a 3D train driving
simulation like OpenBVE, but implemented as a game with scoring
mechanics rather than as a pure simulation experience. Environments and
routes are built-in, and users can unlock new scenarios by scoring
points within previous scenarios, which are awarded based on adherence
to the schedule for that scenario and the accuracy of the player's
stopping position when arriving at stations.

Like DDG, my solution will use timetable data extensively. However,
because players are responsible for controlling trains in DDG rather
than the simulation itself, actual departure and arrival times within
the game differ from the scheduled times by varying amounts. Even the
most timely real-world trains are also late sometimes, so I think this
property would be good to include in my solution for the sake of
realism. Instead of relying on human error and inconsistency, my
simulation will simply offset arrival and departure times by a random
value based on realistic error margins.

DDG includes a single camera control button that toggles between the
default in-cab view and an alternate position predetermined by the game
based on the train's current position. For example, when approaching a
station, the alternate position might jump ahead to the station and
rotate to track the trains movement. Another alternate position might
follow the train from outside with a fixed rotation, or provide a
bird's-eye-view of the train. None of these options allow the player to
freely move or rotate the camera, but the variety and dynamic nature of
the alternative camera positions does keep the gameplay interesting.

# Essential Features

The primary function of the solution is to display trains within a 3D
environment of a real-world location, applying actual timetable data to
infer and set the position of each train in real-time. While train
models aren't necessarily known in advance, the simulation will at least
display trains using 3D models of rolling stock that could actually be
used for the corresponding real-world train. Trains within the
simulation will move in a realistic manner, such as accelerating and
decelerating with accurate timing, speed, and positioning during their
journey. Each of these variables will also be slightly offset by a
random amount to add variation because, in reality, trains are rarely
exactly on time or perfectly consistent.

Each of these features is intended to bring as much accuracy to the
simulation as possible, because railfans, the solution's main target
audience, would likely be annoyed by any mismatches. The final product
will include at least one railway line or section of line implemented to
these standards.

When the user selects a train or station by clicking on it within the
simulation, information about it will be displayed in a simple, easy to
read dialog. For example, clicking on a train would bring up data about
the line it's on, its model name, service name, destination, and links
to relevant external information. This adds functionality to the
application that could be useful even to people who aren't particularly
interested in trains but do utilise them.

To widen the accessibility and appeal of the solution, it will include
simple UI or keyboard controls for adjusting the speed and current time
of the simulation. Users will be able to, for example, double or half
the speed scale or jump ahead by an hour as many times as they want,
with the current time and speed states being displayed in the UI.

Limitations

As it is a simulation, my solution will be unable to replicate the real
world completely accurately. One particular limitation is that trains
beginning and ending their journey will fade in and out of thin air,
rather than being deployed from or retired to a depot as they would be
in reality. This is to limit the scope of the project, as implementing
this accurately would require a lot of work to ensure collisions are
avoided and that the correct amount of trains are spawned. A more basic,
inaccurate implementation would be possible, but would not contribute
positively to solving the problem.

Any delays to trains that occur in reality would not be reflected in the
simulation. While this feature would benefit the solution, it would
greatly increase the project's complexity and the real-time delay
information needed to implement isn't available for most locations.

Another limitation is that only a small amount of locations will be
available in the simulation, which could restrict interest in the
solution to those with an interest in those locations. This is, again,
necessary to limit the scope of the project. While most of the work is
likely to be in reusable code and components that can be used across all
locations, implementing new places will still take a significant amount
of time particularly due to the research needed to maintain accuracy.

# System Requirements

+-----------------------------------+-----------------------------------+
| ## Requirement                    | ## Justification                  |
+-----------------------------------+-----------------------------------+
| Basic I/O peripherals (monitor,   | Required to be able to view and   |
| keyboard, and mouse)              | interact with the running         |
|                                   | software                          |
+-----------------------------------+-----------------------------------+
| Graphical Linux environment       | Operating system to be supported  |
|                                   | and tested for during development |
+-----------------------------------+-----------------------------------+
| Internet connection               | Required for streaming 3D map     |
|                                   | data                              |
+-----------------------------------+-----------------------------------+
| 200MB free disk space             | Required to store engine runtime  |
|                                   | and map/timetable data            |
+-----------------------------------+-----------------------------------+
| 2GB RAM                           | Basic system requirements         |
+-----------------------------------+-----------------------------------+
| x86_64 CPU                        | Basic system requirements         |
+-----------------------------------+-----------------------------------+
| GPU with support for OpenGL or    | Basic system requirements         |
| Vulkan                            |                                   |
+-----------------------------------+-----------------------------------+

# Success Criteria

  -----------------------------------------------------------------------
  Trains accurately depart and arrive at stations according to the
  timetable.

  Users can freely control their perspective within the simulation, as
  well as attach it to a specific train.

  Users can adjust the speed at which the simulation runs.

  Users can offset the simulation's time by specific amounts both
  forwards and backwards.

  Displays key information about stations and trains when the user clicks
  on them, including the name of the subject, the railway line(s) it is
  on, and upcoming timetable data for the current simulation time.

  Shows current information about the simulation, including time and
  speed scale, as an overlay UI.

  The UI is subtle and unobtrusive, meaning it uses neutral colours,
  occupies a small amount of the screen, and only at edges or corners of
  the screen.

  3D models used for trains within the same scene follow a consistent
  style and depict a class of rolling stock that would be used on the
  same part of the same line in the real world.

  The environmental 3D models used in a scene follow a consistent style,
  and are detailed enough for someone familiar with the location (or with
  access to a photo) to tell that they match.
  -----------------------------------------------------------------------

# Design

# Breaking down the problem

![](images/image7.png){width="6.080246062992126in"
height="4.572851049868766in"}

I identified four main parts of my solution's main problem, laid out in
the diagram above.

## Storing track configuration data

In order to display trains at the correct position in the simulation
environment, directly over the appropriate tracks, the simulation will
need to know exactly where tracks are as well as information like their
shape, what stations they connect to, and their intended travel
direction. Because this information isn't included as part of the
timetable data or any other source that I know of, I will have to
manually define track information as part of a local data source that
the final program will have access to. This process will take place
during development of the solution rather than being performed at
runtime as part of the solution itself.

Track data will be split into sections that go from station to station
so that the track distance between stations can be easily calculated.
This also removes the need for explicit markers for where the train
stopping point is for each station, as the end of each track section can
simply be used instead. In cases where there are multiple possible
stopping points in the same track section, such as for two-way
single-track sections, I will store any additional points as distance
values measured from one end of the track section. Track sections will
also store the unique IDs of the stations it runs between (the same IDs
used by the station and timetable data).

To store the configuration of each section of track, I will use splines,
which are a common method for representing complex curve configuration
in software. Most game engines have functionality for easily defining
splines in 2D or 3D space, which will be useful to me for lining up the
curves with the tracks displayed in the environment.

The data for each section of track will be stored as part of the
environment itself, rather than a centralised array or map. At runtime,
when looking for a specific track section, the program will be able to
iterate over each section and check if either of the stations it
connects to is the one it's looking for. As an optimisation, I will most
likely perform this process as soon as station data is loaded by storing
references to track sections in the data for each station, as track data
will be needed very frequently and indirectly accessing them won't be
very fast.

## 

## Retrieving timetable data

There will be two possible sources for retrieving the timetable data to
be used in the simulation, depending on the environment that is being
loaded. If the location and services in the environment have a timetable
that is freely provided by an online API, then the program will fetch
the latest data from that API at runtime. This is so that the simulation
is as accurate as possible to the real world. However, if there is no
such API, then I will manually find and include the timetable values in
the simulation as a file during development.

After loading the data, either from a local file or remote API, the
program will need to parse the data into its own data structures so that
it can be used in the simulation. Because the format of the data
provided by each API may be different, I will need to implement the code
for parsing on a case-by-case basis for each API depending on its
format. Only a single implementation is needed if the data was retrieved
from a local file created during development. I will use a simple JSON
or YAML format, as those standards are fairly human-readable and
human-writable, and can be used out-of-the-box in many programming
languages.

The algorithms I will use to accomplish this task, as well as the two
tasks below, are described in the algorithms section.

## Determining train position at a given time and track section

This is part of the same process as 'placing a train at a given position
within the simulation', but is distinguished by the fact that it only
performs calculations for the train's position and other state, without
changing the simulation environment itself. These calculations may also
be needed for other processes. For example, if I were to implement a
display of a particular train's current speed, I would use part of these
same calculations.

The program must find the data values it needs to perform the
calculations before it can begin. These include values for the maximum
acceleration, deceleration, and speed of the train's class, which will
be stored statically within the program itself, and the time between the
train's last scheduled departure and next scheduled arrival, which are
parsed from timetable data and later referenced in the train's own data
structure. In practice, some of these values may not always be needed
depending on the train's current state, but this will depend on the
final implementation.

Using equations of motion along with the above values, then simulation
can determine the position and speed the train will be travelling with
at any given time. The train's position, in particular, will be used in
the next step of placing trains in the simulation.

## Placing a train at given position within the simulation

Given the type of train it needs to place, the track section it is
currently in, and the train's position in that track section as a
proportion of the section's length, the simulation will position each of
the train's carriages in 3D space, ensuring all bogies are properly
placed over the track and facing forwards. This process will be
performed for each train every time the simulation's time is updated,
and is important to get right because its result is displayed directly
to the user and must accurately reflect the available timetable data in
order to solve my problem.

# Usability Features

The main view of the 3D simulation will be overlaid by a UI providing
the user with information, including the current time and speed scale,
as well as controls to adjust the time and speed freely.

The most important feature of the UI is displaying the current time, so
this element will be the most prominent. While the time could be
visualised using an analogue-style clock, a text-based digital display
is more suited to my solution because it would be easier to make compact
without sacrificing readability, and would also be easier to
cross-reference against times from elsewhere (such as train times
displayed in the application).

Additionally, the interface must display the current speed scale of the
simulation. This doesn't need to be particularly large or emphasised,
because the user will only reference it when changing the speed
manually, unlike time which progresses on its own and will likely be
checked often.

The last feature required is the ability to adjust the time and speed of
the simulation. In order to allow fine but also quick and convenient
adjustments, multiple buttons that shift time and speed by certain
increments will be displayed, such as 1 hour, 10 minutes, and 30 seconds
for time.

# Interface design

![](images/image8.png){width="5.0in"
height="2.8125in"}

![](images/image9.png){width="5.0in"
height="2.8125in"}

![](images/image10.png){width="5.037175196850393in"
height="2.8229166666666665in"}

I created two mock-ups for the overlay interface, one with all content
in a bar on the left-hand side of the window and another with the bar
horizontally across the bottom. Each one has the same content laid out
in a logical order, with the time in the most prominent position and the
other information and controls filling the space below it.

The increased available space with the side-bar design does allow for a
slightly more practical experience as the sections are more visually
distinct, but I don't think it's worth it at the cost of covering more
of the view of the simulation. The bottom-bar design offers a similar
layout but takes up considerably less of the window due to being laid
out in the same direction as English text, although the side-bar could
be just as space-efficient if it were displayed in a language that
allows vertical text. This problem could be worked around by displaying
any long text in a tooltip popup on mouse hover, but this would decrease
the ease of learning and use for the interface somewhat without any
significant benefit.

The third UI mock-up shows the time, but with the control bar hidden.
This could be done with either the side or bottom bar by simply showing
the bar if the mouse is brought up to that edge of the screen, but
wouldn't be necessary with the bottom bar anyway as it takes up little
space in the first place. However, a button to temporarily hide the
control bar could still be useful to the user, for example if they
wanted to take a clean screenshot of the simulation without the
interface getting in the way.

Regardless of the final layout, the interfaces in the solution must be
readable no matter what is being displayed in the simulation. Because
the user can control the camera freely, meaning that the simulation
could show anything from a blue sky to a dark building, all text must be
placed over a contrasting background, which is already the case in the
mock-ups (except for the time in two of them).

# Variables and data structures

struct Station {

> val name: string
>
> val schedule: Map\<Time, TrainServiceInfo\>

}

The information stored in the Station struct is only needed for direct
display to the user. Its *schedule* value stores the times of train
services that stop at the station throughout the day.

class TrainService {

> val info: TrainServiceInfo
>
> val class: TrainClass
>
> val schedule: Map\<Time, Station\>
>
> val arrival_offsets: Map\<Time, Duration\>
>
> val departure_offsets: Map\<Time, Duration\>
>
> fun getPosition(time: Time, apply_offsets: bool): TrainPosition

}

TrainService contains information about a single, specific train that
runs through the stations in its *schedule* at specific times. The
*arrival_offsets* and *departure_offsets* values are the random
pregenerated values used to slightly offset the arrival and departure
times from each station. These must be stored persistently rather than
being generated and used on the spot, because they will be used in
calculations for train positions. The calculations are performed
multiple times, and would not be consistent if the time offsets changed
each time.

struct TrainPosition {

> val previous_station: Station
>
> val next_station: Station
>
> val progress: float

}

The TrainPosition struct will be outputted by the function that
calculates a train's position at a given time. It includes references to
the stations that the train is in between (allowing the current track
section to be found), as well as the train's position between those
stations as a ratio of the total track length.

enum TrainServiceType {

LOCAL, RAPID, EXPRESS // etc.

}

struct TrainServiceInfo {

> val type: TrainServiceType
>
> val origin: Station
>
> val destination: Station

}

The data in TrainServiceInfo describes a train service that might run
multiple times throughout a day. It includes references to the origin
and destination stations, as well as the type of the service (such as
local or express) using the TrainServiceType enum.

struct TrainClass {

> val name: string
>
> val maximum_speed: float
>
> val acceleration: float
>
> val deceleration: float
>
> val distance_between_carriages: Float

}

TrainClass stores information about a specific physical class of train
including its name, as well as its maximum speed and acceleration which
will be used in position calculations.

# Program structure

# ![](images/image11.png){width="5.104460848643919in" height="8.646332020997376in"}

# Algorithms

The most complex algorithms that I will need to implement for the
solution are for loading and appropriately formatting the simulation's
timetable data, using that data to determine the positions of each train
at any given time, and then displaying trains accurately in the 3D
environment at their calculated position. Each of these is outlined in
the program structure diagram above, and described in more detail below:

## Loading resources

This will be done a single time when the simulation is loaded, in order
to retrieve and parse any information needed for it to run including
station and timetable data. The loaded data will be stored in memory
throughout the program's execution.

![](images/image12.png){width="5.784571303587051in"
height="5.945597112860892in"}

![](images/image13.png){width="5.394432414698163in"
height="2.674739720034996in"}

## Determining train positions

The first section of this algorithm prepares the data values needed
later on, including the train's schedule, its previous and next station,
and the time it departed the previous station. In addition, it uses
these values to find the time between departure and the next arrival as
well as the total track distance between the stations.

![](images/image14.png){width="5.726599956255468in"
height="4.939192913385827in"}

The second part of the algorithm is responsible for calculating the
train's current position between the previous and next stations based on
the time since departure and the train class's acceleration,
deceleration, and maximum speed. The algorithm uses equations of motion
to consider what stage of its journey the train is in, and then
determines the distance it has travelled since departing. While the code
below calculates the train's position based on the highest possible
speed, I may change the final implementation to use the minimum speed
required to reach the next station by the scheduled arrival time
(although this would simply be a stylistic change).

![](images/image15.png){width="6.039720034995626in"
height="5.385417760279965in"}

Finally, the result of the calculation (the current distance from the
previous station) is converted to a fraction of the total distance
between the two stations and returned along with references to those
stations using the TrainPosition struct. This result will mainly be used
in the next algorithm, for placing trains in the simulation.

![](images/image16.png){width="5.0in"
height="1.28125in"}

## 

## Placing trains in simulation world

Using the train position calculated in the previous algorithm, this
algorithm places a train's model in the simulation at that position and
aligns each carriage and bogey with the track's shape. The first bogey
of the first carriage (from the front) is placed at the exact calculated
position, and each subsequent bogey is placed at the point on the track
that is the correct distance away from the last.

![](images/image17.png){width="6.267716535433071in"
height="6.083333333333333in"}![](images/image18.png){width="6.267716535433071in"
height="0.7222222222222222in"}

# Identifying test data

## Input validation

There are only a few inputs that will need validation while the solution
is in use. From the user, the only possible inputs are interactions with
the simulation controls (such as speed adjustment), moving the camera,
and clicking on objects for further information. All of these are simple
actions that don't involve any data from the user beyond the location
and nature of the interaction itself, so the only validation needed is
for things like keeping the camera in-bounds and whether or not a click
on the simulation actually falls over an object with additional viewable
information.

However, the simulation will use timetable data from an external source
which will need to be validated as it contains complex information and
is required for the simulation to function at all. While being parsed
into the data structures described earlier, the program will have to
check that the data returned by the API or file contains all needed keys
and that each value is of the expected type. If invalid data is found,
the program should try to make do with the valid data it has as much as
possible. If too much of the data is faulty, then details about what is
wrong with the data as well as what was expected instead should be
provided to the user so that they can attempt to resolve the issue.

## 

## Testing method

Each part of the solution will be tested thoroughly during its
development to ensure it is functional. For example, while writing the
function for loading timetable data I will test it multiple times with
different inputs and check that the output is valid. This type of
testing is informal, but can be carried out easily and reduces the risk
of things breaking later on during development. Screenshots and written
records of testing during development will be kept for some of the most
important parts of the solution, as well as any notable instances where
my testing helps me resolve an issue. This evidence will show that my
testing method is effective at identifying whether a section of code is
functional.

In addition to this, I will perform a series of structured and
documented tests covering the entire solution once development is
completed or near-completion, listed below. For each of these tests,
evidence will be collected that shows that the test was performed, the
result of the test, and any conclusions made and/or action taken based
on the test result.

+-------------+-----------------+--------------------+-----------------+
| **Test      | **Input**       | **Expected         | **Further       |
| name**      |                 | output**           | details**       |
+-------------+-----------------+--------------------+-----------------+
| Check that  | 1\. No cache    | 1\. Simulation     |                 |
| the         |                 | starts             |                 |
| simulation  | 2\. Valid cache |                    |                 |
| loads       |                 | 2\. Simulation     |                 |
| s           | 3\. Invalid     | starts             |                 |
| uccessfully | cache           |                    |                 |
| or, if      |                 | 3\. Error          |                 |
| input is    | 4\. No cache,   | containing         |                 |
| invalid,    | no Internet     | location and       |                 |
| fails       | connection      | details of failure |                 |
| gracefully  |                 | within file        |                 |
|             |                 |                    |                 |
|             |                 | 4\. Error stating  |                 |
|             |                 | that an internet   |                 |
|             |                 | connection is      |                 |
|             |                 | required           |                 |
+-------------+-----------------+--------------------+-----------------+
| Check basic | Move and rotate | Camera can be      |                 |
| camera      | camera using    | moved left, right, |                 |
| controls    | mouse and       | forwards,          |                 |
|             | keyboard        | backwards, up, and |                 |
|             | (WASD + space + | down, and can be   |                 |
|             | shift)          | rotated freely     |                 |
|             |                 | using the mouse.   |                 |
+-------------+-----------------+--------------------+-----------------+
| Check extra | Attach camera   | Camera follows     |                 |
| camera      | to train by     | movement of        |                 |
| controls    | clicking on     | attached train,    |                 |
|             | appropriate     | and rotation can   |                 |
|             | button, then    | be controlled      |                 |
|             | rotate camera   | using the mouse    |                 |
|             | using mouse     |                    |                 |
+-------------+-----------------+--------------------+-----------------+
| Check       | Press each      | Simulation time    | Simulation time |
| simulation  | button for      | inc                | can be observed |
| time        | increasing and  | rements/decrements | using the       |
| controls    | decreasing      | by the correct     | displayed time  |
|             | simulation time | amount for each    | as well as the  |
|             |                 | button pressed     | movement of     |
|             |                 |                    | objects in the  |
|             |                 |                    | simulation      |
+-------------+-----------------+--------------------+-----------------+
| Check       | Press each      | Simulation speed   | \"              |
| simulation  | button for      | inc                |                 |
| speed       | increasing and  | rements/decrements |                 |
| controls    | decreasing      | by the correct     |                 |
|             | simulation      | amount for each    |                 |
|             | speed           | button pressed     |                 |
+-------------+-----------------+--------------------+-----------------+
| Check       | 1x speed, 2x    | Simulation time    | \"              |
| simulation  | speed, 0.5x     | progresses on its  |                 |
| progression | speed           | own at the correct |                 |
|             |                 | rate for the       |                 |
|             |                 | current speed      |                 |
+-------------+-----------------+--------------------+-----------------+
| Check       | Click on train  | When clicking on a |                 |
| timetable   | or station      | train or station,  |                 |
| display     |                 | timetable data     |                 |
|             |                 | corresponding to   |                 |
|             |                 | the clicked object |                 |
|             |                 | is displayed       |                 |
+-------------+-----------------+--------------------+-----------------+
| Check train | None            | Trains depart and  |                 |
| timing      |                 | arrive at stations |                 |
|             |                 | at the correct     |                 |
|             |                 | time based on the  |                 |
|             |                 | timetables         |                 |
|             |                 | displayed within   |                 |
|             |                 | the simulation     |                 |
+-------------+-----------------+--------------------+-----------------+
| Check train | None            | Trains are always  |                 |
| positioning |                 | placed over a      |                 |
|             |                 | track, with        |                 |
|             |                 | carriages spaced   |                 |
|             |                 | correctly and      |                 |
|             |                 | bogeys rotated to  |                 |
|             |                 | match the track    |                 |
|             |                 | direction          |                 |
+-------------+-----------------+--------------------+-----------------+

# Implementation

# Project scope

Before starting development, considering the time I had left to complete
the project, I made the decision to scale back the project's scope by
implementing it as a top-down 2D simulation instead of having a 3D
environment. I felt that this would allow me to meet the majority of my
success criteria while also cutting down on a significant amount of
complexity which could otherwise prevent me from completing the project
at all.

# Parsing and displaying railway lines data

I started by downloading a section of northern Osaka city from
OpenStreetMap, which comes in an XML format aptly named osm. An osm file
is an unordered list of objects or points in the map called nodes (an
example of which is shown below).

+-----------------------------------------------------------------------+
| \<node id=\"4427192486\" visible=\"true\" version=\"5\"               |
| changeset=\"76073233\" timestamp=\"2019-10-23T00:24:49Z\"             |
| user=\"okadatsuneo\" uid=\"10163840\" lat=\"34.7205345\"              |
| lon=\"135.4822731\"\>                                                 |
|                                                                       |
| \<tag k=\"name\" v=\"十三\"/\>                                        |
|                                                                       |
| \<tag k=\"name:en\" v=\"Jūsō\"/\>                                     |
|                                                                       |
| \<tag k=\"network\"                                                   |
| v=\"阪急電鉄神戸線;阪急電鉄宝塚線;阪急電鉄京都線\"/\>                 |
|                                                                       |
| \<tag k=\"operator\" v=\"阪急電鉄\"/\>                                |
|                                                                       |
| \<tag k=\"public_transport\" v=\"station\"/\>                         |
|                                                                       |
| \<tag k=\"railway\" v=\"station\"/\>                                  |
|                                                                       |
| \<tag k=\"toilets:wheelchair\" v=\"yes\"/\>                           |
|                                                                       |
| \<tag k=\"train\" v=\"yes\"/\>                                        |
|                                                                       |
| \<tag k=\"wheelchair\" v=\"yes\"/\>                                   |
|                                                                       |
| \<tag k=\"wikidata\" v=\"Q866163\"/\>                                 |
|                                                                       |
| \<tag k=\"wikipedia\" v=\"ja:十三駅\"/\>                              |
|                                                                       |
| \</node\>                                                             |
+-----------------------------------------------------------------------+

In addition to nodes, an osm file also contains structures called ways
and relations. A way is a list of nodes used to describe a short route,
such as a short road or section of track. A relation is usually a larger
group of ways and nodes, such as a railway line. Railway relations
usually contain multiple nodes (stations) and ways (short track
sections). Each of these three data types are uniquely identifiable and
store information about themselves such as names and location.

Once I understood how the map format was structured, I wrote a class for
parsing the raw map data into objects that could be used in the
simulation called OsmLoader.

![](images/image19.png){width="6.267716535433071in"
height="4.902777777777778in"}

OsmLoader provides a single, simple function for loading a specific
file. The file is loaded into a single **Map** object which can then be
read by the user. Because OSM files contain several different data
structures, I chose to split the class's actual parsing logic into three
functions, each for a different type of data.

![](images/image20.png){width="5.933503937007874in"
height="7.786458880139983in"}

One of these functions, parseWay, takes the XML parser from loadOsmFile
as an argument and continues parsing each line of the XML file until the
end of the current XML node. populating the Map object with the data
found. The two other parsing functions follow a similar structure, but
for parsing basic nodes and relations.

To test OsmLoader, I wrote a simple scene that loads a map file at
runtime and displays it using simple lines and icons for stations. A
comparison of the in-engine map display against a real-world map of the
same location is shown below.

![](images/image21.png){width="4.864583333333333in"
height="4.739583333333333in"}

![](images/image22.png){width="2.963542213473316in"
height="2.396510279965004in"}![](images/image23.png){width="3.0364588801399823in"
height="2.3869061679790025in"}

# Parsing and displaying train timetable data![](images/image24.png){width="3.0677088801399823in" height="5.074076990376203in"}

After completing OsmLoader, I collected the timetable data for the
railway lines on the map and stored it using a simple JSON format.
Beside is a sample of the data for the Hankyu Takarazuka line. The
top-level structure is an array that holds every train that runs on that
line on a weekday, where each item holds information about the train's
service type and each of its stops.

In order to hold this data in the program, I created a global class
(equivalent to a namespace) containing several structural classes for
storing timetable data.

-   Timetable: Top-level class for storing stations and trains

-   Station: A single station with name information and a schedule (the
    latter is currently unused)

-   Stop: Comprised of an arrival and departure time, as well as the
    station

-   TrainService: A single train with a linear schedule of stops as well
    as a type and class

-   TrainClass: Physical characteristics of a train such as maximum
    speed and acceleration to be used when displaying a train

Most of these are based closely on the 'Variables and data structures'
specification in the design section, with the main changes being the
addition of **TrainStop** (due to my timetable data containing arrival
times in addition to departure times) and the removal of
**TrainServiceInfo** (the information in which can be inferred from the
train's schedule).

The TT namespace containing each data structure also holds the function
loadTimetableFile which takes a file and a reference to a Timetable
structure. loadTimetableFile parses the file's content and stores it in
the passed Timetable.

![](images/image25.png){width="6.168907480314961in"
height="6.007341426071741in"}

![](images/image26.png){width="5.371350612423447in"
height="7.545468066491688in"}

# 

# Calculating on-map train positions

I then wrote the TrainPosition class for storing a single train's
position between two stations at a given moment.

![](images/image27.png){width="5.25in"
height="1.0416666666666667in"}

#### The class also includes two static functions for calculating a TrainPosition given a train's timetable data and a specific time.

## 

## calculateTrainPosition

This takes a TrainService (containing timetable data) and timestamp, and
outputs a TrainPosition struct containing the train's previous stop,
next stop, and the fraction of the distance between them it has
travelled.

It works by checking each pair of stops in the train's schedule until it
either finds a pair that the current time falls between, or finds a
station that the train is currently stopped at.

## calculateMapTrainPositions

calculateMapTrainPositions takes a timestamp, as well as a Map data
structure, a Timetable, and a dictionary allowing easy access to the
corresponding MapNode (from Map) of each Station (from Timetable). It
then returns a dictionary containing the Vector2 position of each active
train in the timetable, using calculateTrainPosition in the process.

This involves some maths including calculating the total length of track
between pairs of station nodes, and calculating the physical position of
a train on a section of track based on its progress fraction.

![](images/image28.png){width="6.5in"
height="8.86111111111111in"}

![](images/image29.png){width="6.5in"
height="8.541666666666666in"}

# Displaying trains using calculated positions

Once I'd written the logic and structures for calculating the position
of each train, I added a basic implementation for displaying each active
position on the in-game map of stations and track.

Doing this was fairly simple, all I needed to do was recalculate the
position of each train on every frame and render a marker at each frame.
A more efficient way of doing this would be to keep track of the
currently active trains and only check those, as well as the next train
to become active, but I didn't think this would be necessary for a
project of this scale, and it probably would've been a lot of work.

I was able to implement the brute-force method without harming
performance by moving expensive calculations into a separate thread
which continually updates a dictionary containing train positions, which
is then independently rendered each frame with a simple **draw_circle**
call (for testing). I also added a time variable which is updated each
frame and used for position calculations, as well as a simple display of
the current simulation time in the corner of the
screen.![](images/image30.png){width="5.588542213473316in"
height="5.896045494313211in"}

![](images/image31.png){width="4.88334864391951in"
height="4.799008092738408in"}

![](images/image32.png){width="4.88584208223972in"
height="0.9328576115485564in"}

![](images/image33.png){width="6.5in"
height="3.6527777777777777in"}

In the above image, five separate trains can be seen. These animate
smoothly along the track lines based on the simulation time, visible in
the bottom-left corner.

There were still a few bugs at this stage though. Most notably (and
visible in the image), some trains don't travel along the correct track.
This is an issue of this specific section of rail where three lines (for
six total tracks) run parallel between three stations. I believed it was
caused by an oversight in the code that finds the map node of each
timetable station, in which stations can match nodes with the same name
even if they aren't on the same railway line.

I was able to solve this issue by creating a third data source to act as
a bridge between the timetable data and map data. The main part of this
is a dictionary of names of railway lines in the map data, and their
corresponding names in the timetable data as well as their direction of
travel.

Each railway line is given a reference to its information from the
dictionary after being parsed in OsmLoader.parseRelation, as shown
below.

![](images/image34.png){width="4.864583333333333in"
height="4.4375in"}

![](images/image35.png){width="4.731232502187226in"
height="1.6740080927384078in"}

I then updated the setTimetable function in MapDisplay to store each map
station corresponding to its direction, and updated the
calculateMapTrainPositions function to use the correct station for the
direction of each train.

![](images/image36.png){width="6.267716535433071in"
height="5.416666666666667in"}

![](images/image37.png){width="6.267716535433071in"
height="4.277777777777778in"}

As an initial implementation of the visuals for trains, I added a simple
coloured rectangle to each train and then added code for setting train
rotations along with their positions. I first added a new property to
the TrainPosition class for storing an instantaneous rotation. Then, to
the calculateMapTrainPositions function, I added a 'tangents' array
alongside the 'distances' array, in which the angle of the line between
each node is stored. Later in the function, when the train's current
line segment is found, the angle for that segment is set as the
TrainPosition's rotation. This rotation is applied to the train visual
at the same time as the position.

![](images/image38.png){width="5.330949256342957in"
height="3.938859361329834in"}

![](images/image39.png){width="2.8489588801399823in"
height="2.9772069116360456in"}

With the simulation itself completed, I then added the user interface
for controlling the simulation time and speed, closely following the
design I created during the project's design stage. Like the initial
design, the final interface includes buttons for toggling whether the
simulation is paused, buttons for adding or subtracting time, and
buttons for adding or subtracting simulation speed.

One minor change I made was to the increment values of the buttons,
particularly those of the speed buttons. Instead of adding or
subtracting speed in fairly small increments (20x, 5x, and 0.5x) like in
the plan, I realised that larger values that would multiply or divide
the current speed would be more useful for speed control. Users might
want to look for a specific time to view the simulation of but, for
speed, I think large increments are more appropriate so that the speed
can be easily scaled in large amounts.

The screenshots below show the use of the '+10s' button to adjust the
current time while the simulation is paused: starting at 24 seconds,
then incrementing once to 34 seconds using a left-click, then
decrementing twice down to 14 seconds using right-clicks.

![](images/image40.png){width="6.5in"
height="0.7742246281714785in"}

![](images/image41.png){width="6.5in"
height="0.7392016622922135in"}

![](images/image42.png){width="6.5in"
height="0.7791174540682415in"}

![](images/image43.png){width="6.5in"
height="0.764928915135608in"}

Writing the UI code went smoothly and I didn't run into any issues, but
one minor bug I noticed after finishing its implementation
(specifically, when I took screenshots for the example above) was that
changes to the simulation time didn't apply to the map while the
simulation was paused. The simple fix for this was to tweak the main
loop (\_process) so that while paused, instead of skipping updates to
both the current time value and the map itself, it only skips the
simulation's time value increment.

![](images/image44.png){width="4.463793744531934in"
height="1.757341426071741in"}

# Evaluation

# Testing to inform evaluation

As the first part of my evaluation, I used the testing plan I created in
the design stage to ensure that the final program is functional and
robust. As I completed each test, I matched it against relevant success
criteria and determined if the test evidence showed that the criterion
was met.

+---+------------------------------------------------------------------+
| # | ## Criterion                                                     |
| # |                                                                  |
|   |                                                                  |
| N |                                                                  |
| o |                                                                  |
| . |                                                                  |
+---+------------------------------------------------------------------+
| 1 | Trains accurately depart and arrive at stations according to the |
|   | timetable.                                                       |
+---+------------------------------------------------------------------+
| 2 | Users can freely control their perspective within the            |
|   | simulation, as well as attach it to a specific train.            |
+---+------------------------------------------------------------------+
| 3 | Users can adjust the speed at which the simulation runs.         |
+---+------------------------------------------------------------------+
| 4 | Users can offset the simulation's time by specific amounts both  |
|   | forwards and backwards.                                          |
+---+------------------------------------------------------------------+
| 5 | Displays key information about stations and trains when the user |
|   | clicks on them, including the name of the subject, the railway   |
|   | line(s) it is on, and upcoming timetable data for the current    |
|   | simulation time.                                                 |
+---+------------------------------------------------------------------+
| 6 | Shows current information about the simulation, including time   |
|   | and speed scale, as an overlay UI.                               |
+---+------------------------------------------------------------------+
| 7 | The UI is subtle and unobtrusive, meaning it uses neutral        |
|   | colours, occupies a small amount of the screen, and only at      |
|   | edges or corners of the screen.                                  |
+---+------------------------------------------------------------------+
| 8 | 3D models used for trains within the same scene follow a         |
|   | consistent style and depict a class of rolling stock that would  |
|   | be used on the same part of the same line in the real world.     |
+---+------------------------------------------------------------------+
| 9 | The environmental 3D models used in a scene follow a consistent  |
|   | style, and are detailed enough for someone familiar with the     |
|   | location (or with access to a photo) to tell that they match.    |
+---+------------------------------------------------------------------+

## Check that the simulation loads successfully or, if input is invalid, fails gracefully

While the original plan for this test was to check data loading with or
without cache and with or without an internet connection, the final
implementation for data loading uses a static file stored locally
instead of loading from the internet at runtime. Therefore, I performed
just two tests: one with valid data files and one with damaged data
files.

### Test #1 - Valid files

Test: Open with valid data files

Result: Expected (program loads successfully)

Evidence:

![](images/image45.png){width="3.029255249343832in"
height="1.7100634295713035in"}

### Test #2 - Invalid files

Test: Open with an invalid data file

Result: Expected (program displays error message with problem details)

Evidence:

![](images/image46.png){width="6.267716535433071in"
height="3.5277777777777777in"}

## Check basic camera controls

Because the project was scaled down from 3D to 2D, this test now only
involves camera movement using the keyboard instead of that as well as
rotation using the mouse.

Test: Press WASD keys on keyboard

Result: Expected (camera moves left, right, up, or down respectively
when WASD key(s) pressed

Evidence:

![](images/image47.png){width="3.0677088801399823in"
height="1.725586176727909in"}![](images/image48.png){width="3.0781255468066493in"
height="1.7257688101487314in"}

![](images/image49.png){width="3.0781255468066493in"
height="1.727001312335958in"}![](images/image50.png){width="3.0677088801399823in"
height="1.7179166666666668in"}

Relevant success criteria:

> **2. Users can freely control their perspective within the simulation,
> as well as attach it to a specific train.**
>
> The evidence shows that the WASD keys can be used to freely control
> the movement of the camera, partially fulfilling this criterion.

## Check extra camera controls

Test: Click on train to open details menu, click 'Follow' button

Result: Expected (camera follows train)

Evidence:

![](images/image51.png){width="4.131297025371828in"
height="2.326428258967629in"}

![](images/image52.png){width="4.151209536307961in"
height="2.335718503937008in"}

![](images/image53.png){width="4.161458880139983in"
height="2.3434120734908137in"}

Relevant success criteria:

> **2. Users can freely control their perspective within the simulation,
> as well as attach it to a specific train.**
>
> The evidence shows that the user can click on a train to have the
> camera follow it, partially fulfilling this criterion.

## Check simulation time controls

Test: Left-click and right-click each time adjustment button

Result: Expected (time increases by labelled amount when left-clicked,
decreases when right-clicked)

Evidence:

![](images/image54.png){width="3.057292213473316in"
height="1.719083552055993in"}![](images/image55.png){width="3.0781255468066493in"
height="1.7180227471566054in"}

![](images/image56.png){width="3.0468755468066493in"
height="1.7122933070866142in"}![](images/image57.png){width="3.026042213473316in"
height="1.7021489501312337in"}

Relevant success criteria:

> **4. Users can offset the simulation's time by specific amounts both
> forwards and backwards.**
>
> The evidence shows that the program displays buttons which increase or
> decrease the current time of the simulation when left-clicked or
> right-clicked respectively. This criterion has been met.

## Check simulation speed controls

Test: Left-click and right-click each speed adjustment button

Result: Expected (speed increases by labelled proportion when
left-clicked, decreases when right-clicked)

Evidence:

![](images/image58.png){width="4.90625in"
height="0.5926727909011373in"}

![](images/image59.png){width="4.901042213473316in"
height="0.5454647856517936in"}

![](images/image60.png){width="4.890625546806649in"
height="0.5686778215223097in"}

![](images/image61.png){width="4.901042213473316in"
height="0.5373239282589676in"}

![](images/image62.png){width="4.901042213473316in"
height="0.5655052493438321in"}

Relevant success criteria:

> **3. Users can adjust the speed at which the simulation runs.**
>
> The evidence shows that the program displays buttons which increase or
> decrease the speed of the simulation when left-clicked or
> right-clicked respectively. This criterion has been met.
>
> **6. Shows current information about the simulation, including time
> and speed scale, as an overlay UI.**
>
> The evidence shows that the program UI accurately displays the current
> speed of the simulation, partially fulfilling this criterion.

## Check simulation progression

### Test #1 - 1x speed

Test: Take screenshots 10 seconds apart at 1x speed

Result: Expected (10 second gap between times displayed in screenshots)

Evidence:

![](images/image63.png){width="6.270833333333333in"
height="0.7335225284339457in"}

![](images/image64.png){width="6.270833333333333in"
height="0.7346478565179353in"}

### Test #2 - 10x speed

Test: Take screenshots 10 seconds apart at 10x speed

Result: Expected (100 second gap between times displayed in screenshots)

Evidence:

![](images/image65.png){width="6.270833333333333in"
height="0.7551290463692039in"}

![](images/image66.png){width="6.270833333333333in"
height="0.7458377077865267in"}

Relevant success criteria:

> **6. Shows current information about the simulation, including time
> and speed scale, as an overlay UI.**
>
> The evidence shows that the program UI accurately displays the current
> time of the simulation, partially fulfilling this criterion.

## Check timetable display

Test: Click on train

Result: Expected (Full timetable of clicked train displayed)

Evidence:

![](images/image67.png){width="6.267716535433071in"
height="3.5277777777777777in"}

Relevant success criteria:

> **5. Displays key information about stations and trains when the user
> clicks on them, including the name of the subject, the railway line(s)
> it is on, and upcoming timetable data for the current simulation
> time.**
>
> The evidence shows that when the user clicks on a train, a panel is
> displayed containing information about the train including its class,
> line, and timetable data. This criterion has been met.
>
> **7. The UI is subtle and unobtrusive, meaning it uses neutral
> colours, occupies a small amount of the screen, and only at edges or
> corners of the screen.**
>
> The evidence shows that, even when fully expanded, the program UI only
> consumes a small amount of screen space, uses neutral grey colours
> (and minimal accents to convey information), and remains unobtrusive
> by both being physically small and having a translucent background.
> This criterion has been fulfilled.

## Check train timing

Test: Pick a train in the simulation, record arrival times at two
stations

Result: Expected (arrival times match those displayed within the
program)

Evidence:

![](images/image68.png){width="6.267716535433071in"
height="3.5277777777777777in"}

Centre train departing 大阪梅田 at 12:10

![](images/image69.png){width="6.267716535433071in"
height="3.5277777777777777in"}

Centre train arriving at 十三 at 12:13

Relevant success criteria:

> **1. Trains accurately depart and arrive at stations according to the
> timetable.**
>
> The evidence shows that trains within the simulation accurately move
> between stations using the timetable data within the program, which
> means this criterion was fully met.

## Check train positioning

This test mostly matches the original plan, sans testing the spacing
between trains and the positioning of individual bogeys (as only a
single carriage with a central pivot was implemented).

Test: Observe train as it moves between two stations

Result: Expected (train moves smoothly between stations staying on the
same line and rotating with the track)

Evidence:

![](images/image70.png){width="3.026042213473316in"
height="1.706172353455818in"}![](images/image71.png){width="3.0468755468066493in"
height="1.7153357392825896in"}

![](images/image72.png){width="3.0156255468066493in"
height="1.694346019247594in"}![](images/image73.png){width="3.026042213473316in"
height="1.7011679790026246in"}

![](images/image74.png){width="2.994792213473316in"
height="1.6864359142607175in"}![](images/image75.png){width="3.0062128171478566in"
height="1.6928674540682416in"}

![](images/image76.png){width="3.057292213473316in"
height="1.7243547681539808in"}![](images/image77.png){width="3.0468755468066493in"
height="1.7157655293088363in"}

# 

# Stakeholder testing

When I'd finished implementing and testing the program, I gave it to two
stakeholders and asked them some questions about their experience.

## Did you have any difficulty in controlling the camera?

**Stakeholder A:**

The camera controls are very simple, just WASD to move and mouse scroll
to zoom, so I found it easy to move the camera how I wanted to.

**Stakeholder B:**

It wasn't immediately clear how to move around, but it wasn't very hard
to figure it out since I have played some games that use the WASD keys.
Zooming with the mouse wheel was intuitive though.

## Did you find the movement of the trains to be accurate to the timetable?

**Stakeholder A:**

Yes, it was satisfying to see the trains move according to the schedule
I know, especially when trains departed simultaneously from the same
station. I also noticed that trains do stop at stations for a minute or
so, which seems accurate.

**Stakeholder B:**

I wasn't familiar with the area shown by the simulation, but when I
compared the movements of the trains to the timetable that showed up
when I clicked on them, they were always on time.

## Did you have any issues when reading or interacting with the program's interface?

**Stakeholder A:**

No, all of the text was well contrasted against their backgrounds and
easy to read. The buttons also stood out well enough and were labelled
clearly.

**Stakeholder B:**

All of the text was very readable and I also appreciated that there were
only a few buttons, which made it easy to keep track of which one did
what.

# Success of the solution

Based on both my own testing as well as the feedback from my
stakeholders, here is a summary of my success criteria and whether I
think the testing shows that they were met, unmet, or partially met.

+---+------------------------------------------+-----------------------+
| # | ## Criterion                             | ## Met?               |
| # |                                          |                       |
|   |                                          |                       |
| N |                                          |                       |
| o |                                          |                       |
| . |                                          |                       |
+---+------------------------------------------+-----------------------+
| 1 | Trains accurately depart and arrive at   | Yes                   |
|   | stations according to the timetable.     |                       |
+---+------------------------------------------+-----------------------+
| 2 | Users can freely control their           | Partially             |
|   | perspective within the simulation, as    |                       |
|   | well as attach it to a specific train.   |                       |
+---+------------------------------------------+-----------------------+
| 3 | Users can adjust the speed at which the  | Yes                   |
|   | simulation runs.                         |                       |
+---+------------------------------------------+-----------------------+
| 4 | Users can offset the simulation's time   | Yes                   |
|   | by specific amounts both forwards and    |                       |
|   | backwards.                               |                       |
+---+------------------------------------------+-----------------------+
| 5 | Displays key information about stations  | Partially             |
|   | and trains when the user clicks on them, |                       |
|   | including the name of the subject, the   |                       |
|   | railway line(s) it is on, and upcoming   |                       |
|   | timetable data for the current           |                       |
|   | simulation time.                         |                       |
+---+------------------------------------------+-----------------------+
| 6 | Shows current information about the      | Yes                   |
|   | simulation, including time and speed     |                       |
|   | scale, as an overlay UI.                 |                       |
+---+------------------------------------------+-----------------------+
| 7 | The UI is subtle and unobtrusive,        | Yes                   |
|   | meaning it uses neutral colours,         |                       |
|   | occupies a small amount of the screen,   |                       |
|   | and only at edges or corners of the      |                       |
|   | screen.                                  |                       |
+---+------------------------------------------+-----------------------+
| 8 | 3D models used for trains within the     | No                    |
|   | same scene follow a consistent style and |                       |
|   | depict a class of rolling stock that     |                       |
|   | would be used on the same part of the    |                       |
|   | same line in the real world.             |                       |
+---+------------------------------------------+-----------------------+
| 9 | The environmental 3D models used in a    | No                    |
|   | scene follow a consistent style, and are |                       |
|   | detailed enough for someone familiar     |                       |
|   | with the location (or with access to a   |                       |
|   | photo) to tell that they match.          |                       |
+---+------------------------------------------+-----------------------+

## Justifying remaining criteria

These criteria were unmentioned or only partially justified in the
testing section above.

**2. Users can freely control their perspective within the simulation,
as well as attach it to a specific train.**

While users are able to control the camera freely, the controls for this
weren't immediately apparent to one stakeholder, so some in-program
guidance on camera controls may have helped. To keep the UI minimal, a
simple label could've been added to the bottom-right corner describing
the controls for camera movement (WASD) and zooming (scroll wheel).

**5. Displays key information about stations and trains when the user
clicks on them, including the name of the subject, the railway line(s)
it is on, and upcoming timetable data for the current simulation time.**

This criterion was only partially met because, while detailed
information is displayed when a train is clicked, the same is not true
for stations. Stations are completely non-interactive in the final
program.

**8. 3D models used for trains within the same scene follow a consistent
style and depict a class of rolling stock that would be used on the same
part of the same line in the real world.**

**9. The environmental 3D models used in a scene follow a consistent
style, and are detailed enough for someone familiar with the location
(or with access to a photo) to tell that they match.**

These two criteria were both unmet because I decided not to implement
the 3D environment, and instead focused on a simpler top-down 2D map.

# Usability of the product

Each element is strongly contrasted against its background. All text is
solid white and backgrounds are dark
greys.![](images/image78.png){width="4.575716316710412in"
height="2.562869641294838in"}

All buttons are clearly labelled, and the somewhat function of
right-clicking on a time/speed button to subtract instead of adding is
indicated by a separate
label.![](images/image69.png){width="1.8893930446194225in"
height="3.596560586176728in"}

The only other interface in the program, the train info display, shows
only the minimum required amount of information to be as readable as
possible: A simple header with a title and close button, the line and
service type of the train, the type of service the train is running, and
the train's scheduled stops. The list of stops is emphasised by a line
which also serves to make it easier to find the train's line using its
colour.

Lastly, at the bottom of the column is a single button for attaching the
camera to the train in question.

Like the main HUD interface, all text is coloured white, which contrasts
well against the main grey background as well as the container
backgrounds of the header and each stop element.

In general, the program is simple to use. No setup is required, and
running it immediately opens the simulation view. In addition, users
with some technical knowledge could replace the data files used for the
map and for timetables, although this isn't documented or indicated
anywhere and wouldn't be considered a feature in this state.

# Future maintenance and development

Because the program has relatively few features, and these features are
all fairly simple, the program would be easy to maintain in its current
state in the future. For example, things like updating timetable data or
using a larger map area would be trivial to implement, because the
program loads this data externally using standard formats. However, this
also means that if one source of this data were to disappear, or if its
provided format were to change, new code would have to be rewritten to
support a new source and/or format. That being said, if this were to
happen, only a small part of the program would have to be revised,
because the external data is parsed and loaded into the program's own
data structures before being provided to the simulation itself, meaning
that only the code used for parsing and loading the data would need
updating.

On the other hand, if the program were to be expanded in the future
instead of simply being maintained in its current state, such as by
adding larger map areas with more railways, things would become more
difficult. The program was designed and implemented with only a small
map area in mind, and as such uses a brute force algorithm for
calculating train positions which made it much easier to implement but
would likely lead to performance issues when used for areas with a large
number of trains to keep track of. If this path were to be taken, large
parts of the simulation code would need to be rewritten and its
structure would have to change significantly. Instead of checking every
single train when updating the map, the algorithm would have to be aware
of which trains were visible on the previous update and only check those
trains (and the next ones to appear based on the timetable).

As for smaller improvements, there are many that could be made fairly
easily, such as adding a background to the map using satellite imagery,
or displaying trains using actual top-down images which would both
improve the program visually, an area that is particularly lacking at
the moment. One function that was planned in the design stage but not
implemented was an information display for stations. A feature like this
would be especially easy to add by reusing the existing UI that is
currently used for trains, as they would share several fields such as
arrival times and the name of the line they are on.

Finally, the main part of my success criteria that I was not able to
meet was the 3D display of simulated trains. If this were implemented,
the code for parsing data and the core simulation algorithm for
calculating train positions could be kept as-is while just replacing
code for the visuals and adding any extra logic required to the
simulation such as for vertical components of train positions.

# References

Godot Engine. (2023, 8 23). Retrieved from Godot Engine:
https://github.com/godotengine/godot/releases/tag/4.1-stable

Ibamoto. (2023, 11 1). *新大阪駅全景（2020年3月）*. Retrieved from
Wikimedia Commons:
https://commons.wikimedia.org/wiki/File:IBA-Shinosaka-panoramic-view-2020.jpg

KOI, H. (2023, 9 20). *metrogram*. Retrieved from nulldesign.jp:
https://nulldesign.jp/metrogram/

OpenBVE. (2023, 9 20). *OpenBVE Project*. Retrieved from OpenBVE:
https://openbve-project.net/

OpenStreetMap Foundation. (2024, 2 13). Retrieved from OpenStreetMap:
https://www.openstreetmap.org/

TAITO CORPORATION. (2023, 9 20). 電車でGO! ポケット大阪環状線編.
