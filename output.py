import os

f = open("files", "r")
files = [line.strip() for line in f.readlines() if not line.startswith("#")]
f.close()

for file_path in files:
	f = open(file_path, "r")
	data = f.read()
	f.close()
	print(file_path.replace("./", "/<root>/"))
	print(data)

