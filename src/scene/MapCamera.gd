extends Camera2D
class_name MapCamera

const SPEED: float = 1000

signal ZoomLevelChanged(float)

@onready var _base_parent: Node2D = get_parent()
var _current_follow_train: TrainPreview = null

func _getMovementSpeed() -> float:
	return SPEED / zoom.x

func _process(delta: float):
	if Input.is_action_pressed("camera_up"):
		position.y -= _getMovementSpeed() * delta
	if Input.is_action_pressed("camera_down"):
		position.y += _getMovementSpeed() * delta
	if Input.is_action_pressed("camera_left"):
		position.x -= _getMovementSpeed() * delta
	if Input.is_action_pressed("camera_right"):
		position.x += _getMovementSpeed() * delta
	
	if Input.is_action_just_released("camera_zoom_in"):
		zoom.x *= 1.1
		zoom.y *= 1.1
		
		ZoomLevelChanged.emit(zoom.x)
	
	if Input.is_action_just_released("camera_zoom_out"):
		zoom.x *= 0.9
		zoom.y *= 0.9
		
		ZoomLevelChanged.emit(zoom.x)

func setFollowTrain(train: TrainPreview):
	if _current_follow_train != null:
		_current_follow_train.reset.disconnect(onFollowTrainReset)
	_current_follow_train = train
	
	var target_pos: Vector2
	var target_node: Node2D
	
	if train == null:
		target_node = _base_parent
		target_pos = global_position
	else:
		target_node = train
		target_pos = train.global_position
		train.reset.connect(onFollowTrainReset)
	
	get_parent().remove_child(self)
	target_node.add_child(self)
	
	global_position = target_pos

func onFollowTrainReset():
	setFollowTrain(null)
