class_name TrainPreview
extends Node2D

signal reset

@onready var visual: Area2D = $Visual

var train_position: TrainPosition
var current_rotation: float = null

func setPositionMeta(position: TrainPosition):
	if position == null:
		if train_position != null:
			train_position == null
			_reset()
		return
	
	if train_position != null and train_position.train != position.train:
		_reset()
	
	train_position = position
	
	if position.rotation_rads != current_rotation:
		if current_rotation == null:
			visual.rotation = position.rotation_rads
		else:
			create_tween().tween_property(visual, "rotation", position.rotation_rads, 0.5)
		current_rotation = position.rotation_rads

func setCameraZoom(zoom: Vector2):
	var x = max(1, 1 / zoom.x)
	var y = max(1, 1 / zoom.y)
	scale = Vector2(x, y)

func _reset():
	current_rotation = null
	reset.emit()

func isTouchingCursor() -> bool:
	return visual.has_overlapping_bodies()
