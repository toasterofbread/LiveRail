extends Node2D

const MAP_FILE: String = "res://map.osm"
const TIMETABLE_FILES: Array[String] = ["res://data/19500.json", "res://data/19800.json", "res://data/20000.json"]
const MAX_TIME: float = 24 * 60 * 60

@onready var hud = $CanvasLayer/Hud
@onready var map_display = $MapDisplay
@onready var error_label: Label = $CanvasLayer/ErrorLabel

var loader: OsmLoader = OsmLoader.new()
var timetable: TT.Timetable = TT.Timetable.new()

var paused: bool = false
var simulation_speed: float = 1
var simulation_time_s: float = (12 * 60 + 10) * 60

func _ready():
	for file in TIMETABLE_FILES:
		var error: String = TT.loadTimetableFile(
			FileAccess.open(file, FileAccess.READ), 
			timetable
		)
		
		if error != null:
			error_label.text = error
			error_label.visible = true
			return
	
	loader.loadOsmFile(MAP_FILE)
	$MapDisplay.setMap(loader.map)
	$MapDisplay.setTimetable(timetable)
	
	add_child(CursorCollisionBody.new())

func _process(delta: float):
	if Input.is_action_just_pressed("toggle_pause"):
		paused = !paused
	
	if not paused:
		simulation_time_s = fmod(simulation_time_s + (delta * simulation_speed), MAX_TIME)
	
	hud.setTime(simulation_time_s)
	map_display.setTime(simulation_time_s)

func onHudTimeChanged(by_s):
	simulation_time_s += by_s

func onHudSpeedChanged(by_x: float):
	if by_x < 0:
		by_x = 1.0 / -by_x
	simulation_speed *= by_x
	hud.setSpeed(simulation_speed)

func onHudPauseToggled():
	paused = not paused
	hud.setPaused(paused)
