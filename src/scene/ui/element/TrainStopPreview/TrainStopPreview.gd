extends Control
class_name TrainStopPreview

func setStop(stop: TT.Stop):
	var station_name: String = stop.station.name
	var bracket_index: int = station_name.find("(")
	if bracket_index > -1:
		station_name = station_name.substr(0, bracket_index)
	
	$Label.text = station_name
	
	var time: float
	if stop.arrival != null:
		time = stop.arrival
	else:
		time = stop.departure
	
	if time != null:
		$Label.text += " (" + TT.secondsToText(time) + ")"
