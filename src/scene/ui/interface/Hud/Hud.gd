extends Control
class_name Hud

signal time_changed(by_s: int)
signal speed_changed(by_x: int)
signal pause_toggled

@export var map: MapDisplay

@onready var time_display: Control = $VBoxContainer/MarginContainer/HBoxContainer/TimeDisplay
@onready var speed_label: Label = $VBoxContainer/PanelContainer/HBoxContainer/PanelContainer2/SpeedButtons/HBoxContainer/SpeedLabel
@onready var info_display: InfoDisplay = $VBoxContainer/MarginContainer/HBoxContainer/InfoDisplay

@onready var time_buttons: Control = $VBoxContainer/PanelContainer/HBoxContainer/PanelContainer/TimeButtons
@onready var speed_buttons: Control = $VBoxContainer/PanelContainer/HBoxContainer/PanelContainer2/SpeedButtons
@onready var pause_button: Button = $VBoxContainer/PanelContainer/HBoxContainer/PauseButton

func _ready():
	for button in time_buttons.get_children():
		if not button is Button:
			continue
		
		var delta_s: float = float(button.name.replace("_", "."))
		connectButtonPress(
			button,
			func(right_click):
				if (right_click):
					time_changed.emit(-delta_s)
				else:
					time_changed.emit(delta_s)
		)

	for button in speed_buttons.get_children():
		if not button is Button:
			continue
		
		var delta_x: float = float(button.name.replace("_", "."))
		connectButtonPress(
			button,
			func(right_click):
				if (right_click):
					speed_changed.emit(-delta_x)
				else:
					speed_changed.emit(delta_x)
		)

func connectButtonPress(button: Button, onPressed: Callable):
	button.gui_input.connect(
		func(event: InputEvent):
			if not event is InputEventMouseButton or not event.pressed:
				return
			
			if event.button_index == 1:
				onPressed.call(false)
			elif event.button_index == 2:
				onPressed.call(true)
	)

func onPauseButtonPressed():
	pause_toggled.emit()

func setPaused(paused: bool):
	if paused:
		pause_button.text = "Resume simulation"
	else:
		pause_button.text = "Pause simulation"

func setTime(time_s: float):
	time_display.setTime(time_s)

func setSpeed(speed_x: float):
	speed_label.text = str(speed_x) + "x"

func onInfoDisplayFollowRequested(train: TrainPreview):
	map.camera.setFollowTrain(train)
