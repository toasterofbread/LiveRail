extends PanelContainer
class_name InfoDisplay

signal follow_requested(train: TrainPreview)

const TRAIN_STOP_PREVIEW_SCENE: PackedScene = preload("res://src/scene/ui/element/TrainStopPreview/TrainStopPreview.tscn")

@onready var train_stop_list: VBoxContainer = $VBoxContainer/PanelContainer/MarginContainer/VBoxContainer/ScrollContainer/HBoxContainer/MarginContainer/TrainStopPreviewContainer
@onready var service_type_label: Label = $VBoxContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/MarginContainer/HBoxContainer/ServiceTypeLabel

@onready var line_name_label: Label = $VBoxContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/MarginContainer/HBoxContainer/LineNameLabel
@onready var line_indicators: Array = [
	$VBoxContainer/PanelContainer/MarginContainer/VBoxContainer/ScrollContainer/HBoxContainer/TrainLineIndicator1,
	$VBoxContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/TrainLineIndicator2,
	$VBoxContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/TrainLineIndicator3,
	$VBoxContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/TrainLineIndicator4
]

var _current_train_preview: TrainPreview = null

func _ready():
	visible = false
	
	var close_button: TextureButton = $VBoxContainer/HBoxContainer/CloseButton
	close_button.texture_normal = ThemeDB.get_default_theme().get_icon("close", "Icons")

func hideDisplay():
	visible = false
	_current_train_preview = null

func displayTrain(train_preview: TrainPreview):
	_current_train_preview = train_preview
	
	var train: TT.TrainService = train_preview.train_position.train
	
	line_name_label.text = train.line.name
	service_type_label.text = TT.getTrainServiceTypeName(train.type)
	
	for indicator in line_indicators:
		indicator.color = train.line.map_line.colour
	
	for stop in train_stop_list.get_children():
		stop.queue_free()
	
	for stop in train.stops:
		var preview: TrainStopPreview = TRAIN_STOP_PREVIEW_SCENE.instantiate()
		preview.setStop(stop)
		train_stop_list.add_child(preview)
	
	visible = true

func onCloseButtonPressed():
	hideDisplay()

#func _process(_delta: float):
#	if _current_train_preview == null:
#		return

func onFollowButtonPressed():
	if _current_train_preview != null:
		follow_requested.emit(_current_train_preview)
